import math


# *********************************************************
def multiply_all(*args, **kwargs):
    """
    Args:
        *args (int|float):
        **kwargs (dict): values are int or float
    Returns:
        multiplied result
    """

    if (0 in args) or (0 in kwargs.values()):
        return 0

    all_args = list(args) + list(kwargs.values())
    result = 1
    for arg in all_args:
        result *= arg
    return result


def multiply_all_2(*args, **kwargs):
    if (0 in args) or (0 in kwargs.values()):
        return 0

    all_args = list(args) + list(kwargs.values())
    result = math.prod(all_args)
    return result


print(multiply_all(1, 2, 3, 4, 5.3, n=2))
print(multiply_all_2(1, 2, 3, 4, 5.3, n=2))


# *********************************************************

def get_rectangle_info(a, b, type: str = 'p'):
    """
    Args:
        a (int|float):
        b(int|float):
        type (str): 'p' - periment, 'd' - the diameter of the circumscribed circle, 's' - square
    Returns:
        'p' - periment, 'd' - the diameter of the circumscribed circle, 's' - square or None
    """

    if type == 'p':
        return (a + b) * 2
    elif type == 'd':
        return (a ** 2 + b ** 2) ** 0.5
    elif type == 's':
        return a * b
    else:
        return None


print(get_rectangle_info(3, 4, 'd'))

# **************************************************


pupils = [
    {'first_name': 'Paul', 'second_name': 'Clinton', 'age': 8, 'math': 10, 'history': 7},
    {'first_name': 'Sean', 'second_name': 'Clinton', 'age': 9, 'math': 10, 'history': 7},
    {'first_name': 'Paul', 'second_name': 'Bush', 'age': 8, 'math': 10, 'history': 7},
    {'first_name': 'Sara', 'second_name': 'Clinton', 'age': 8, 'math': 11, 'history': 7},
    {'first_name': 'Mark', 'second_name': 'Loren', 'age': 8, 'math': 10, 'history': 7},
    {'first_name': 'Emily', 'second_name': 'Clinton', 'age': 8, 'math': 10, 'history': 7},
    {'first_name': 'Bill', 'second_name': 'Trump', 'age': 8, 'math': 10, 'history': 7},
    {'first_name': 'Paul', 'second_name': 'Li', 'age': 8, 'math': 10, 'history': 7},
    {'first_name': 'Paul', 'second_name': 'Chak', 'age': 8, 'math': 10, 'history': 7},
]


def has_namesake(school_pupils):
    names = []
    for puple in school_pupils:
        if puple['first_name'] not in names:
            names.append(puple['first_name'])
        else:
            return True
    return False


print(has_namesake(pupils))

# *********************************************
# brackets

def is_balanced(text, brackets="<>()[]{}"):
    opening, closing = brackets[::2], brackets[1::2]
    stack = []
    for character in text:
        if character in opening: #
            stack.append(opening.index(character))
        elif character in closing:
            if stack and stack[-1] == closing.index(character):
                stack.pop()
            else:
                return False 

    return (not stack)

print(is_balanced("(5+5)/([[4+4]]*{2*2})"))
print(is_balanced("(3+[2*3)]"))

# **********************************************

